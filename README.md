# Wallet_Machine_Code



## Getting started
This is a java based project which contains basic functionality of the wallet.
All the data created will be stored in-memory;

## Language Used
Java 8

## API
1. Register User
2. Create wallet
3. Load money into the wallet
4. Fetch wallet's balance.
5. Send money to other user.
6. Fetch transactions hostory based on date and user_id
7. Apply offer for a transaction, if applicable. 
