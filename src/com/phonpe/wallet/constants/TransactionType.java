package com.phonpe.wallet.constants;

public enum TransactionType {
    LOAD_MONEY,
    SEND_MONEY;
}
