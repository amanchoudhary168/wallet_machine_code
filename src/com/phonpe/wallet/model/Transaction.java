package com.phonpe.wallet.model;

import com.phonpe.wallet.constants.TransactionStatus;
import com.phonpe.wallet.constants.TransactionType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class Transaction {
    private String transactionId;
    private Date transactionDate;
    private TransactionType transactionType;
    private String payerId;
    private String payeeId;
    private BigDecimal amount;
    private TransactionStatus status;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public String getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }


}
