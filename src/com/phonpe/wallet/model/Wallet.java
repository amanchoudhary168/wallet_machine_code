package com.phonpe.wallet.model;

import com.sun.xml.internal.ws.util.ReadAllStream;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class Wallet {
    String walletId;
    String userId;
    Wallet type;
    BigDecimal amount;
    ReadWriteLock lock;
    Wallet status;

    public String getWalletId() {
        return walletId;
    }

    public String getUserId() {
        return userId;
    }

    public Wallet getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ReadWriteLock getLock(){
        return this.lock;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setType(Wallet type) {
        this.type = type;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setLock(ReadWriteLock lock){
        this.lock = lock;
    }
}
