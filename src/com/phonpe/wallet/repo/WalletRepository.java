package com.phonpe.wallet.repo;

import com.phonpe.wallet.model.User;
import com.phonpe.wallet.model.Wallet;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class WalletRepository {

    Map<String, Wallet> walletMap = new ConcurrentHashMap<>();
    AtomicInteger counter =new AtomicInteger(1);

    public String  createWallet(Wallet wallet){
        String walletId  =  String.valueOf(counter.getAndIncrement());
        wallet.setWalletId(walletId);
        wallet = walletMap.putIfAbsent(walletId,wallet);
        if(wallet == null)
                return walletId;
        return wallet.getWalletId();
    }

    public Wallet getWallet(String walletId){
        return walletMap.get(walletId);
    }
}
