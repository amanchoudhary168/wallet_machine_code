package com.phonpe.wallet.repo;

import com.phonpe.wallet.model.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class TransactionRepo {
    Map<String, Transaction> transactionMap = new ConcurrentHashMap<>();
    AtomicInteger counter =new AtomicInteger(1);

    public Transaction saveTransaction(Transaction transaction){
        String transactionId  =  String.valueOf(counter.getAndIncrement());
        transaction.setTransactionId(transactionId);
        transactionMap.putIfAbsent(transactionId,transaction);
        return transaction;
    }

    public List<Transaction> getAllTransactions(){
        Set<Map.Entry<String,Transaction>> entries = transactionMap.entrySet();
        return entries.stream().map(e->e.getValue()).collect(Collectors.toList());
    }

    public Transaction updateTransaction(Transaction t){
        transactionMap.put(t.getTransactionId(),t);
        return t;
    }



}
