package com.phonpe.wallet.repo;

import com.phonpe.wallet.model.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class UserRepository {
    Map<String, User> userMap = new ConcurrentHashMap<>();
    AtomicInteger counter =new AtomicInteger(1);

    public String  createUser(User user){
        String userId = String.valueOf(counter.getAndIncrement());
        user.setUserId(userId);
        user = userMap.putIfAbsent(userId,user);
        if(user == null)
            return userId;
        return user.getUserId();
    }

    public User getUser(String userId){
        return userMap.get(userId);
    }


    public String  updateUser(User user){
        userMap.put(user.getUserId(),user);
        return user.getUserId();
    }


}
