package com.phonpe.wallet.service;

import com.phonpe.wallet.model.Transaction;
import com.phonpe.wallet.repo.TransactionRepo;
import com.phonpe.wallet.service.api.ITransactionService;

import java.util.List;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 06-06-2023
 **/
public class TransactionServiceImpl implements ITransactionService {


    private TransactionRepo transactionRepo;

    public TransactionServiceImpl(TransactionRepo transactionRepo) {
        this.transactionRepo = transactionRepo;
    }

    @Override
    public Transaction addTransaction(Transaction t) {
       return transactionRepo.saveTransaction(t);
    }

    @Override
    public Transaction updateTransaction(Transaction t) {
       return transactionRepo.updateTransaction(t);
    }

    @Override
    public List<Transaction> getAllTransaction(Transaction t) {
        return transactionRepo.getAllTransactions();
    }
}
