package com.phonpe.wallet.service;

import com.phonpe.wallet.constants.TransactionStatus;
import com.phonpe.wallet.model.*;

import com.phonpe.wallet.service.api.ITransactionService;
import com.phonpe.wallet.service.api.IUserService;
import com.phonpe.wallet.service.api.IUserTransactionService;

import java.math.BigDecimal;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class UserTransactionService implements IUserTransactionService {

    private IUserService userService;
    private WalletService walletService;
    private ITransactionService transactionService;
    private OfferService offerService;

    public UserTransactionService(IUserService userService, WalletService walletService, ITransactionService transactionService,OfferService offerService) {
        this.userService = userService;
        this.walletService = walletService;
        this.transactionService = transactionService;
        this.offerService = offerService;
    }

    @Override
    public String sendMoney(String payer, String payee, BigDecimal amount) {

        if(amount.intValue()<0)
            throw new RuntimeException("Amount can not be 0 or negative");
        User objPayer = userService.getUser(payer);
        User objPayee = userService.getUser(payee);
        Transaction transaction = new Transaction();
        transaction.setPayeeId(payee);
        transaction.setPayerId(payer);
        transaction.setAmount(amount);
        transaction.setStatus(TransactionStatus.PENDING);
        Transaction newTransaction = transactionService.addTransaction(transaction);
        boolean payerDebit = false;
        try{
                 walletService.debit(objPayer.getWalletId(),amount);
                 payerDebit = true;
                 if(payerDebit)
                 walletService.loadMoney(objPayee.getWalletId(),amount);
                 newTransaction.setStatus(TransactionStatus.SUCCESS);
        }catch(Exception e){
            if(payerDebit)
                walletService.loadMoney(objPayer.getWalletId(),amount);
            newTransaction.setStatus(TransactionStatus.FAILED);
        }
        transactionService.updateTransaction(newTransaction);
        offerService.applyOffer(transaction);
        return newTransaction.getTransactionId();

    }
}
