package com.phonpe.wallet.service;

import com.phonpe.wallet.model.Transaction;
import com.phonpe.wallet.repo.TransactionRepo;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class TransactionHistoryService {
    TransactionRepo transactionRepo ;

    public TransactionHistoryService(TransactionRepo repo){
        this.transactionRepo = repo;
    }

    public List<Transaction> fetchTransactionByUserId(String userId){
       return transactionRepo.getAllTransactions().stream().
                filter(e->e.getPayerId().equalsIgnoreCase(userId)).collect(Collectors.toList());
    }

    public List<Transaction> fetchTransactionByUserIdSortedByDate(String userId){
        List<Transaction> userTransactions =  transactionRepo.getAllTransactions().stream().
                filter(e->e.getPayerId().equalsIgnoreCase(userId)).collect(Collectors.toList());
        Collections.sort(userTransactions,(e1,e2)-> e1.getTransactionDate().compareTo(e2.getTransactionDate()));
        return userTransactions;
    }
}
