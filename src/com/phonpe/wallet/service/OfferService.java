package com.phonpe.wallet.service;

import com.phonpe.wallet.model.Transaction;
import com.phonpe.wallet.repo.TransactionRepo;

import java.util.stream.Collectors;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class OfferService {

    TransactionRepo repo;


    public OfferService(TransactionRepo repo) {
        this.repo = repo;
    }

    public void applyOffer(Transaction transaction){

        String userId = transaction.getPayerId();
        if(transaction.getAmount().intValue()>=100){
            long count = repo.getAllTransactions().stream().
                    filter(e->e.getPayerId().equalsIgnoreCase(userId) && e.getAmount().intValue()>=100).count();
            if(count == 1){
                //Add cashBack to the user account;
            }

        }
    }
}
