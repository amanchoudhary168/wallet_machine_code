package com.phonpe.wallet.service;

import com.phonpe.wallet.model.User;
import com.phonpe.wallet.repo.UserRepository;
import com.phonpe.wallet.service.api.IUserService;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class UserService implements IUserService {

    WalletService walletService;
    UserRepository userRepository;

    public UserService(WalletService walletService,UserRepository userRepository){
        this.userRepository  = userRepository;
        this.walletService = walletService;
    }
    @Override
    public String registerUser(String name, String email, String password, String mobileNo) {
       User user = new User();
       user.setUserEmail(email);
       user.setUserName(name);
       user.setUserPhoneNo(mobileNo);
       user.setPassword(password);
       String userId  = userRepository.createUser(user);
       String walletId = walletService.createWallet(userId);
       User upDateUser = userRepository.getUser(userId);
       upDateUser.setWalletId(walletId);
       userRepository.updateUser(upDateUser);
       return userId;
    }

    @Override
    public User getUser(String userId) {
        return userRepository.getUser(userId);
    }

}
