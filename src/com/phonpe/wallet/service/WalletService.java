package com.phonpe.wallet.service;
import com.phonpe.wallet.model.Wallet;
import com.phonpe.wallet.repo.WalletRepository;
import com.phonpe.wallet.service.api.IWalletService;

import java.math.BigDecimal;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public class WalletService  implements IWalletService {

    private WalletRepository walletRepository;

    public WalletService(WalletRepository repository){
        this.walletRepository = repository;
    }

    @Override
    public String createWallet(String userId) {

       Wallet wallet = new Wallet();
       wallet.setAmount(new BigDecimal(0));
       wallet.setUserId(userId);
       return walletRepository.createWallet(wallet);

    }

    @Override
    public BigDecimal fetchBalance(String walletId) {
        Wallet wallet = walletRepository.getWallet(walletId);
        if(wallet == null)
            throw new RuntimeException("Wallet doesn't exits");
        try{
            wallet.getLock().readLock().lock();
            return  wallet.getAmount();
        }finally{
            wallet.getLock().readLock().lock();
        }


    }

    @Override
    public BigDecimal loadMoney(String walletId,BigDecimal amount) {
        Wallet wallet = walletRepository.getWallet(walletId);
        if(amount.doubleValue()<0)
            throw new RuntimeException("Cannot load negative amount");
        if(wallet == null)
            throw new RuntimeException("Wallet doesn't exits");
        synchronized (wallet){
            BigDecimal currentAmount = wallet.getAmount();
            wallet.setAmount(currentAmount.add(amount));
            return wallet.getAmount();
        }
    }

    @Override
    public Wallet fetchWalletDetails(String walletId) {
        return walletRepository.getWallet(walletId);
    }

    @Override
    public void debit(String walletId,BigDecimal amount) {
        Wallet wallet = walletRepository.getWallet(walletId);
        try{
           wallet.getLock().writeLock().lock();
           if(wallet.getAmount().compareTo(amount)< 0)
               throw new RuntimeException("Not Enough Balance");
           wallet.setAmount(wallet.getAmount().subtract(amount));
        }finally {
            wallet.getLock().writeLock().unlock();
        }
    }


}
