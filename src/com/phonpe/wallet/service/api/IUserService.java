package com.phonpe.wallet.service.api;

import com.phonpe.wallet.model.User;

/**
 * @author : aman.chouudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public interface IUserService {

    public String registerUser(String name, String email, String password, String mobileNo);
    public User getUser(String userId);
}
