package com.phonpe.wallet.service.api;

import com.phonpe.wallet.model.Wallet;

import java.math.BigDecimal;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public interface IWalletService {
    public String  createWallet(String userId);
    public BigDecimal fetchBalance(String walletId);
    public BigDecimal loadMoney(String walletId,BigDecimal amount);
    public Wallet fetchWalletDetails(String walletId);
    public void debit(String walletId,BigDecimal amount);
}
