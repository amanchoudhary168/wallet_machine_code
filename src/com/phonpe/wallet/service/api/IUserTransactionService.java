package com.phonpe.wallet.service.api;

import java.math.BigDecimal;

/**
 * @author : aman.choudhary
 * @version : 1.0.0
 * date : 02-06-2023
 **/
public interface IUserTransactionService {
    public String sendMoney(String payer, String payee, BigDecimal amount);

}

