package com.phonpe.wallet.service.api;

import com.phonpe.wallet.model.Transaction;

import java.util.List;

/**
 * @author : aman.chouudhary
 * @version : 1.0.0
 * date : 06-06-2023
 **/
public interface ITransactionService {
    public Transaction addTransaction(Transaction t);
    public Transaction updateTransaction(Transaction t);
    public List<Transaction> getAllTransaction(Transaction t);
}
